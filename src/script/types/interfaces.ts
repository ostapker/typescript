interface IFighter {
    _id: string,
    name: string,
    source: string
}

interface IFighterDetails extends IFighter {
    health: number,
    attack: number,
    defense: number
}

interface IDictionary {
    [index: string]: string
}

interface IDomElement {
    tagName : string, 
    className? : string, 
    attributes? : IDictionary
}

interface IModal {
    title : string,
    bodyElement: HTMLElement
}
