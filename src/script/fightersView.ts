import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService'

export function createFighters(fighters : IFighter[]) : HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

//const fightersDetailsCache = new Map();

async function showFighterDetails(_event: Event, fighter : IFighter ) {
  try {
    const fullInfo = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
  }
  catch(err){
    console.log('fighter not found!');
  }
}

export async function getFighterInfo(fighterId : string) {
    return await getFighterDetails(fighterId)
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetails>();

  return async function selectFighterForBattle(event : Event, fighter : IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = fight(...selectedFighters.values());
      console.log(`Winner: ${winner.name}`);
      showWinnerModal(winner);
    }
  }
}
