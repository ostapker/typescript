const minChance = 1;
const maxChance = 2;

export function fight(firstFighter? : IFighterDetails , secondFighter? : IFighterDetails) : IFighterDetails {
  var attacker = firstFighter;
  var enemy = secondFighter;
  console.log(`${firstFighter?.name}(${firstFighter?.health} hp) vs ${secondFighter?.name}(${secondFighter?.health} hp)`);

  if(enemy!.health <= 0)
    return attacker!;

  var firstFighterHealth = firstFighter?.health;
  var secondFighterHealth = secondFighter?.health;

  while(attacker!.health > 0){
    var damage = getDamage(attacker!, enemy!);
    
    enemy!.health -= damage;
    console.log(`damage:${damage} ${enemy?.name}(${enemy?.health} hp)`);

    var temp = attacker;
    attacker = enemy;
    enemy = temp;
  }
  
  firstFighter!.health = firstFighterHealth!;
  secondFighter!.health = secondFighterHealth!;

  return enemy!;
}

export function getDamage(attacker : IFighterDetails, enemy : IFighterDetails) : number {
  var damage = getHitPower(attacker) - getBlockPower(enemy)
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter : IFighterDetails) : number {
  return fighter.attack * getRandomArbitrary(minChance, maxChance);
}

export function getBlockPower(fighter : IFighterDetails) : number {
  return fighter.defense * getRandomArbitrary(minChance, maxChance);
}

function getRandomArbitrary(min : number, max : number) : number {
  return Math.random() * (max - min) + min;
}