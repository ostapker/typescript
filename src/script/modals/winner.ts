import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter : IFighterDetails) {
  const title = 'Winner winner chicken dinner!';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}


function createWinnerDetails(fighter : IFighterDetails) : HTMLElement {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes : {src: source} });

  nameElement.innerText = name;

  fighterDetails.append(imgElement);
  fighterDetails.append(nameElement);
  
  return fighterDetails;
}