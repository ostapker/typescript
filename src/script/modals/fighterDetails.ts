import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter: IFighterDetails) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter : IFighterDetails) : HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const blockElement = createElement({tagName: 'div'});
  const nameElement = createElement({ tagName: 'p', className: 'name'});
  const attackElement = createElement({ tagName: 'p'});
  const defenseElement = createElement({ tagName: 'p'});
  const healthElement = createElement({ tagName: 'p'});
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes : {src: source} });
  
  nameElement.innerText = name;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defense: ${defense}`;
  healthElement.innerText = `Health: ${health}`;

  fighterDetails.append(imgElement);
  blockElement.append(nameElement);
  blockElement.append(attackElement);
  blockElement.append(defenseElement);
  blockElement.append(healthElement);
  fighterDetails.append(blockElement);
  
  return fighterDetails;
}
